#!/bin/bash

bundle install
bundle exec rake db:create

if [ -e /etc/systemd/system/unicorn.service ]; then
  sudo systemctl stop unicorn
fi

app_name=`pwd | awk -F/ '{print $5}'`
sudo sed -i -e s/rakulacook/$app_name/g ./unicorn.service
sudo cp ./unicorn.service /etc/systemd/system/

sudo systemctl daemon-reload

sudo systemctl start unicorn
sudo systemctl enable unicorn
