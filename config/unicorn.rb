listen "/home/vagrant/unicorn/unicorn.sock"
worker_processes 2
pid "/home/vagrant/unicorn/unicorn.pid"
stderr_path "/home/vagrant/unicorn/log/error.log"
stdout_path "/home/vagrant/unicorn/log/unicorn.log"
